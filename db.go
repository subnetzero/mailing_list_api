package main

import (
	"fmt"

	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"
	rethink "gopkg.in/gorethink/gorethink.v4"
)

// initDatabaseConnection establishes a database session
func initDatabaseConnection(s Specification) *rethink.Session {
	var err error
	var session *rethink.Session
	address := fmt.Sprintf("%v:%d", s.DBHost, s.DBPort)

	connectOpts := rethink.ConnectOpts{
		Address:  address,
		Database: s.DBName,
	}
	if s.DBUsername != "" {
		connectOpts.Username = s.DBUsername
		connectOpts.Password = s.DBPassword
	}

	session, err = rethink.Connect(connectOpts)
	if err != nil {
		log.WithFields(log.Fields{
			"action": "db-connect",
		}).WithError(err).Fatal("Failed to connect to database")
	}

	err = rethink.DB(s.DBName).
		Table(s.DBTable).
		Count().
		Exec(session)
	if err != nil {
		log.WithFields(log.Fields{
			"action": "db-connect",
		}).WithError(err).Fatal("Invalid database access")
	}

	return session
}

// InsertEmailSubscription adds email to the database
func InsertEmailSubscription(s Specification, session rethink.QueryExecutor, email string, unsubToken uuid.UUID) (rethink.WriteResponse, error) {
	// TODO: Remove hardcoded strings
	return rethink.Table(s.DBTable).
		Insert(map[string]string{
			"email":             email,
			"unsubscribe_token": unsubToken.String(),
		}).
		RunWrite(session)
}

// DeleteEmailSubscription removes an email from the database
func DeleteEmailSubscription(s Specification, session rethink.QueryExecutor, token string) (rethink.WriteResponse, error) {
	// TODO: Remove hardcoded strings
	return rethink.Table(s.DBTable).
		GetAllByIndex("unsubscribe_token", token).
		Delete().
		RunWrite(session)
}
