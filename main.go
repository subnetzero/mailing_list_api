package main

import (
	"fmt"
	"net/http"
	"time"

	"github.com/badoux/checkmail"
	"github.com/gorilla/mux"
	"github.com/kelseyhightower/envconfig"
	"github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"
	rethink "gopkg.in/gorethink/gorethink.v4"
)

// Specification is the required environment variables to run this server
type Specification struct {
	Host         string        `required:"true"`
	Port         int           `required:"true"`
	WriteTimeout time.Duration `default:"15s"`
	ReadTimeout  time.Duration `default:"15s"`
	DBHost       string        `required:"true"`
	DBPort       int           `required:"true"`
	DBName       string        `default:"test"`
	DBTable      string        `default:"mailing_list_address"`
	DBUsername   string
	DBPassword   string
}

// SubscribeMessage encapsulates a request to subscribe to the mailing list
type SubscribeMessage struct {
	email string
}

// UnsubscribeMessage encapsulates a request to unsubscribe from the mailing list
type UnsubscribeMessage struct {
	unsubToken string
}

var subChannel chan SubscribeMessage
var unsubChannel chan UnsubscribeMessage

func main() {
	var s Specification
	err := envconfig.Process("mlsub", &s)
	if err != nil {
		log.WithFields(log.Fields{
			"action": "startup",
		}).WithError(err).Fatal("Invalid env config")
	}

	r := mux.NewRouter()
	r.HandleFunc("/subscribe", SubscribeHandler).
		Methods(http.MethodPost)
	r.HandleFunc("/unsubscribe/{token}", UnsubscribeHandler).
		Methods(http.MethodGet)
	r.HandleFunc("/api/health", HealthHandler)
	http.Handle("/", r)

	address := fmt.Sprintf("%v:%d", s.Host, s.Port)
	log.WithFields(log.Fields{
		"action": "startup",
	}).Debug(fmt.Sprintf("Starting server on %v\n", address))
	srv := &http.Server{
		Handler:      r,
		Addr:         address,
		WriteTimeout: s.WriteTimeout,
		ReadTimeout:  s.ReadTimeout,
	}

	var session rethink.QueryExecutor = initDatabaseConnection(s)

	subChannel = make(chan SubscribeMessage)
	unsubChannel = make(chan UnsubscribeMessage)

	go ProcessSubscriptionMessages(s, session)
	go ProcessUnsubscribeMessages(s, session)

	log.WithFields(log.Fields{
		"action": "serve",
	}).Fatal(srv.ListenAndServe())
}

// SubscribeHandler subscribes users to the mailing list if a valid email is supplied in the POST form
func SubscribeHandler(res http.ResponseWriter, req *http.Request) {
	subMsg := SubscribeMessage{req.FormValue("email")} // TODO: remove hardcoded string
	if err := checkmail.ValidateFormat(subMsg.email); err != nil {
		log.WithFields(log.Fields{
			"action": "subscribe",
		}).WithError(err).Info("Subscribe failed on improper formatting")
		res.WriteHeader(http.StatusUnprocessableEntity)
	} else {
		subChannel <- subMsg
		res.WriteHeader(http.StatusOK)
	}
}

// UnsubscribeHandler removes an individual from the mailing list
func UnsubscribeHandler(res http.ResponseWriter, req *http.Request) {
	token := mux.Vars(req)["token"] // TODO: Remove hardcoded string
	unsubChannel <- UnsubscribeMessage{token}
	res.WriteHeader(http.StatusOK)
}

// HealthHandler responds with the health of the server
func HealthHandler(res http.ResponseWriter, req *http.Request) {
	res.WriteHeader(http.StatusOK)
}

// ProcessSubscriptionMessages listens on subChannel and uses messages to add to mailing list
func ProcessSubscriptionMessages(s Specification, session rethink.QueryExecutor) {
	for msg := range subChannel {
		unsubToken, err := uuid.NewV4()
		if err != nil { // Log the error, but proceed
			log.WithFields(log.Fields{
				"action": "subscribe",
			}).WithError(err).Error("Error generating unsubscribe token")
		}

		res, err := InsertEmailSubscription(s, session, msg.email, unsubToken)
		switch {
		case err != nil:
			log.WithFields(log.Fields{
				"action": "subscribe",
			}).WithError(err).Error("Error inserting email into database")
		case res.Errors > 0:
			log.WithFields(log.Fields{
				"action": "subscribe",
			}).Info(fmt.Sprintf("DB write error for subscribe: %v\n", res.FirstError))
			// TODO: Kick off a goroutine to repeat attempts?
		default:
			log.WithFields(log.Fields{
				"action": "subscribe",
			}).Info(fmt.Sprintf("Successfully added record [%v]\n", msg.email))
			// TODO: Send out subscription success email
		}
	}
}

// ProcessUnsubscribeMessages listens on subChannel and uses messages to remove from mailing list
func ProcessUnsubscribeMessages(s Specification, session rethink.QueryExecutor) {
	for msg := range unsubChannel {
		res, err := DeleteEmailSubscription(s, session, msg.unsubToken)
		switch {
		case err != nil:
			log.WithFields(log.Fields{
				"action": "unsubscribe",
			}).WithError(err).Error("Error removing email from database")
		case res.Errors > 0:
			log.WithFields(log.Fields{
				"action": "unsubscribe",
			}).Info(fmt.Sprintf("DB write error for unsubscribe: %v\n", res.FirstError))
			// TODO: Kick off a goroutine to repeat attempts?
		case res.Deleted == 0:
			log.WithFields(log.Fields{
				"action": "unsubscribe",
			}).Info(fmt.Sprintf("Unsubscribe failed: no records found with unsubscribe token [%v]\n", msg.unsubToken))
		default:
			log.WithFields(log.Fields{
				"action": "unsubscribe",
			}).Info("Successfully removed record\n") // TODO: add info on who unsubscribed
		}
	}
}
