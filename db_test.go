package main

import (
	"fmt"
	"testing"

	"github.com/satori/go.uuid"

	"github.com/kelseyhightower/envconfig"
	rethink "gopkg.in/gorethink/gorethink.v4"
)

// TODO: Make sure this is actually testing the code; it might just be
// 		 testing the mocks framework :P
func TestInsertEmailSubscriptionUnique(t *testing.T) {
	var s Specification
	err := envconfig.Process("mlsub", &s)
	if err != nil {
		fmt.Printf("Invalid env config: %v\n", err)
	}

	email := "test@test.com"
	unsubToken, err := uuid.FromString("test-token")
	mockWriteResponse := rethink.WriteResponse{Errors: 0}

	mock := rethink.NewMock()

	mock.On(rethink.Table(s.DBTable).
		Insert(map[string]string{
			"email":             email,
			"unsubscribe_token": unsubToken.String(),
		})).Return([]interface{}{mockWriteResponse}, nil)

	res, err := InsertEmailSubscription(s, mock, email, unsubToken)
	switch {
	case err != nil:
		t.Fail()
	case res.Errors > 0:
		t.Fail()
	default:
		t.Log("Successfully inserted new email subscription record")
	}
}
