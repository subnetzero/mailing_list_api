host=REPLACE_ME
port=REPLACE_ME
dbhost=REPLACE_ME
dbport=REPLACE_ME

all: test build run

build:
	go build
test:
	MLSUB_HOST=${host} MLSUB_PORT=${port} MLSUB_DBHOST=${dbhost} MLSUB_DBPORT=${dbport} go test -v
run:
	MLSUB_HOST=${host} MLSUB_PORT=${port} MLSUB_DBHOST=${dbhost} MLSUB_DBPORT=${dbport} ./mailing_list_api